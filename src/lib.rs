pub use gateway::Gateway;

pub mod calls;
pub mod errors;
mod gateway;
mod html_parser;
pub mod led;
pub mod stats;

/*fn main() {
    let gw = Gateway::new("10.14.2.254");
    println!("{:?}", gw.get_status());
    println!("{:?}", gw.get_line_info());
    println!("{:?}", gw.get_gateway_info());
    println!("{:?}", gw.get_connection_info());
    println!("{:?}", gw.get_call_log());
}*/
