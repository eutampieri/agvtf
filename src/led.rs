use std::convert::TryFrom;

#[derive(Copy, Clone, Debug)]
pub enum Status {
    Green,
    Red,
    Blinking,
    Off,
}

impl<'a> TryFrom<&'a str> for Status {
    type Error = crate::errors::Error;

    fn try_from(value: &'a str) -> Result<Self, Self::Error> {
        match value {
            "ledlight.gif" => Ok(Self::Green),
            "ledlightred.gif" => Ok(Self::Red),
            "ledblinking.gif" => Ok(Self::Blinking),
            "ledoff.gif" => Ok(Self::Off),
            _ => Err(crate::errors::Error::InvalidValue(value.to_owned())),
        }
    }
}

#[derive(Debug)]
pub struct List {
    pub dsl: Status,
    pub internet: Status,
    pub wi_fi: Status,
    pub service: Status,
    pub dect: Status,
    pub line_1: Status,
    pub line_2: Status,
}

impl TryFrom<Vec<Status>> for List {
    type Error = crate::errors::Error;

    fn try_from(value: Vec<Status>) -> Result<Self, Self::Error> {
        match value.len() {
            7 => Ok(List {
                dsl: value[0],
                internet: value[1],
                wi_fi: value[2],
                service: value[3],
                dect: value[4],
                line_1: value[5],
                line_2: value[6],
            }),
            _ => Err(crate::errors::Error::InvalidValue(format!("{:?}", value))),
        }
    }
}
