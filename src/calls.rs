use std::convert::TryFrom;

#[derive(Debug)]
pub enum Call {
    Outbound { from: String, to: String },
    Inbound { from: String, to: String },
}

impl<T> TryFrom<(T, T, T)> for Call
where
    T: AsRef<str>,
{
    type Error = super::errors::Error;
    fn try_from(tuple: (T, T, T)) -> Result<Self, Self::Error> {
        match tuple.2.as_ref() {
            "Ingresso" => Ok(Self::Inbound {
                from: tuple.0.as_ref().to_owned(),
                to: tuple.1.as_ref().to_owned(),
            }),
            "Uscita" => Ok(Self::Outbound {
                from: tuple.1.as_ref().to_owned(),
                to: tuple.0.as_ref().to_owned(),
            }),
            _ => Err(Self::Error::InvalidValue(tuple.2.as_ref().to_owned())),
        }
    }
}
