use std::net::Ipv4Addr;

use crate::errors;

#[derive(Debug)]
pub enum Protocol {
    PPPoE,
    PPPoA,
}

impl std::convert::TryFrom<String> for Protocol {
    type Error = errors::Error;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match value.as_ref() {
            "PPPoE" => Ok(Self::PPPoE),
            "PPPoA" => Ok(Self::PPPoA),
            _ => Err(errors::Error::InvalidValue(value)),
        }
    }
}
#[derive(Debug)]
pub struct GatewayInfo {
    pub model: String,
    pub serial_number: String,
    pub hardware_version: String,
    pub software_version: String,
}
#[derive(Debug)]
pub struct ConnectionInfo {
    //pub profilo_tariffario: String,
    //pub mode: String,
    pub protocol: Protocol,
    //pub connection_mode: String,
    pub ip_address: Ipv4Addr,
    //telegestione
    //connessione automatica da modem
    pub primary_dns: Option<Ipv4Addr>,
    pub secondary_dns: Option<Ipv4Addr>,
    pub other_dns: Vec<Ipv4Addr>,
}

#[derive(Debug)]
pub enum Encapsulation {
    ATM,
    PTM,
}

impl std::convert::TryFrom<&str> for Encapsulation {
    type Error = errors::Error;

    fn try_from(v: &str) -> Result<Self, Self::Error> {
        match v {
            "ATM" => Ok(Self::ATM),
            "PTM" => Ok(Self::PTM),
            _ => Err(Self::Error::InvalidValue(v.to_owned())),
        }
    }
}

#[derive(Debug)]
pub struct TxRx<T>
where
    T: std::ops::Add,
{
    pub tx: T,
    pub rx: T,
}

#[derive(Debug)]
pub struct LineInfo {
    pub max_speed: TxRx<u32>,
    pub dsl_driver_version: String,
    pub encapsulation: Encapsulation,
    pub power: TxRx<f64>,
    pub snr: TxRx<f64>,
    pub speed: TxRx<u32>,
    pub ses: u64,
    pub es: Option<u64>,
    pub fec: Option<u64>,
    pub crc: Option<u64>,
    pub uptime: u64,
    pub inp: u16,
    pub attenuation: TxRx<f64>,
}
