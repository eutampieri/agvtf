use std::fmt::Debug;
use std::{
    convert::{TryFrom, TryInto},
    str::FromStr,
};

use stats::TxRx;

use crate::{calls::Call, errors, html_parser, led, stats};
use chrono::{DateTime, Local, TimeZone};

fn get_from_table<Q: ?Sized, K, V>(
    hm: &std::collections::HashMap<K, V>,
    key: &Q,
) -> Result<V, errors::Error>
where
    Q: Eq + std::hash::Hash + Debug,
    K: std::borrow::Borrow<Q> + Eq + std::hash::Hash,
    V: Clone,
{
    hm.get(key)
        .cloned()
        .ok_or(errors::Error::MissingKey(format!("{:?}", key)))
}

pub struct Gateway {
    ip_addr: String,
}

impl Gateway {
    pub fn new<T>(ip_addr: T) -> Self
    where
        T: AsRef<str>,
    {
        Self {
            ip_addr: String::from(ip_addr.as_ref()),
        }
    }
    pub fn get_status(&self) -> Result<led::List, errors::Error> {
        let response = ureq::get(&format!("http://{}/led_ajax.lp", self.ip_addr))
            .call()?
            .into_string()?;
        let leds: Vec<led::Status> = response
            .trim()
            .split("##")
            .filter_map(|x| led::Status::try_from(x).ok())
            .collect();
        Ok(led::List::try_from(leds)?)
    }

    pub fn get_line_info(&self) -> Result<stats::LineInfo, errors::Error> {
        let response = ureq::get(&format!("http://{}/statisticsAG.lp", self.ip_addr))
            .call()?
            .into_string()?;
        let table = html_parser::parse_table(response);
        Ok(stats::LineInfo {
            max_speed: TxRx {
                tx: get_from_table(&table, "Velocità massima di trasmissione (Kbps):")?.parse()?,
                rx: get_from_table(&table, "Velocità massima di ricezione (Kbps):")?.parse()?,
            },
            dsl_driver_version: get_from_table(&table, "Versione Driver DSL:")?,
            encapsulation: stats::Encapsulation::try_from(
                get_from_table(&table, "Encapsulation Type:")?.as_str(),
            )?,
            power: TxRx {
                tx: get_from_table(&table, "Potenza in trasmissione (dB):")?.parse()?,
                rx: get_from_table(&table, "Potenza in ricezione (dB):")?.parse()?,
            },
            snr: TxRx {
                tx: get_from_table(&table, "SNR Upstream (dB):")?.parse()?,
                rx: get_from_table(&table, "SNR Downstream (dB):")?.parse()?,
            },
            speed: TxRx {
                tx: get_from_table(&table, "Velocità in trasmissione (Kbps):")?.parse()?,
                rx: get_from_table(&table, "Velocità in ricezione (Kbps):")?.parse()?,
            },
            ses: get_from_table(&table, "Total SES (Severely Errored Secs):")?.parse()?,
            uptime: get_from_table(&table, "Up time:")?.parse()?,
            inp: get_from_table(&table, "Impulse Noise Protection (INP):")?.parse()?,
            attenuation: TxRx {
                tx: get_from_table(&table, "Attenuazione Upstream (dB):")?.parse()?,
                rx: get_from_table(&table, "Attenuazione Downstream (dB):")?.parse()?,
            },
            es: get_from_table(&table, "Total ES (Errored Secs):")
                .ok()
                .map(|x| x.parse().ok())
                .flatten(),
            fec: get_from_table(&table, "Total FEC Errors:")
                .ok()
                .map(|x| x.parse().ok())
                .flatten(),
            crc: get_from_table(&table, "Total CRC Errors:")
                .ok()
                .map(|x| x.parse().ok())
                .flatten(),
        })
    }

    fn get_gw_and_conn_info(
        &self,
    ) -> Result<(stats::GatewayInfo, stats::ConnectionInfo), errors::Error> {
        let response = ureq::get(&format!("http://{}/wanStatus.lp", self.ip_addr))
            .call()?
            .into_string()?;
        let table = html_parser::parse_table(response);
        Ok((
            stats::GatewayInfo {
                model: get_from_table(&table, "Modello:")?,
                serial_number: get_from_table(&table, "Seriale Modem:")?,
                hardware_version: get_from_table(&table, "Versione Hardware:")?,
                software_version: get_from_table(&table, "Versione Software:")?,
            },
            stats::ConnectionInfo {
                protocol: get_from_table(&table, "Protocollo di connessione:")?.try_into()?,
                ip_address: std::net::Ipv4Addr::from_str(&get_from_table(
                    &table,
                    "Indirizzo IP Pubblico connessione da modem:",
                )?)?,
                primary_dns: std::net::Ipv4Addr::from_str(&get_from_table(
                    &table,
                    "DNS Primario di default:",
                )?)
                .ok(),
                secondary_dns: std::net::Ipv4Addr::from_str(&get_from_table(
                    &table,
                    "DNS Secondario di default:",
                )?)
                .ok(),
                other_dns: get_from_table(&table, "Altri DNS:")?
                    .split("\n")
                    .map(|x| x.trim())
                    .filter_map(|x| std::net::Ipv4Addr::from_str(x).ok())
                    .collect(),
            },
        ))
    }

    pub fn get_gateway_info(&self) -> Result<stats::GatewayInfo, errors::Error> {
        Ok(self.get_gw_and_conn_info()?.0)
    }
    pub fn get_connection_info(&self) -> Result<stats::ConnectionInfo, errors::Error> {
        Ok(self.get_gw_and_conn_info()?.1)
    }
    pub fn get_call_log(
        &self,
    ) -> Result<Vec<(Call, DateTime<Local>, Option<std::time::Duration>)>, errors::Error> {
        let resp = ureq::get(&format!("http://{}/callLog.lp", self.ip_addr))
            .call()
            .unwrap()
            .into_string()
            .unwrap();
        html_parser::parse_call_log(resp)
            .into_iter()
            .map(|x| ((x.0, x.1, x.2), x.3, x.4))
            .map(|x| {
                (x.0, x.1, {
                    let pieces = x.2.split(':').map(|x| x.parse::<u8>()).collect::<Vec<_>>();
                    if pieces[0].is_ok() {
                        let seconds = pieces
                            .into_iter()
                            .map(|x| x.unwrap() as u64)
                            .enumerate()
                            .map(|(i, x)| x * 60u64.pow(2 - i as u32))
                            .sum();
                        Some(std::time::Duration::from_secs(seconds))
                    } else {
                        None
                    }
                })
            })
            .map(|(c, t, d)| {
                (
                    c,
                    {
                        let mut pieces = t
                            .split(" - ")
                            .map(|x| x.split(':').map(|y| y.parse::<u32>().unwrap()));
                        let mut time = pieces.next().unwrap();
                        let mut date = pieces.next().unwrap().rev();
                        Local
                            .ymd(
                                date.next().unwrap() as i32,
                                date.next().unwrap(),
                                date.next().unwrap(),
                            )
                            .and_hms(
                                time.next().unwrap(),
                                time.next().unwrap(),
                                time.next().unwrap(),
                            )
                    },
                    d,
                )
            })
            .try_fold(vec![], |mut vec, x| {
                let val = Call::try_from(x.0)?;
                vec.push((val, x.1, x.2));
                Ok(vec)
            })
    }
}
