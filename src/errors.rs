use std::{
    net::AddrParseError,
    num::{ParseFloatError, ParseIntError},
};

#[derive(Debug)]
pub enum Error {
    HTTPError(ureq::Error),
    IOError(std::io::Error),
    Generic,
    MissingKey(String),
    ParseIntError(ParseIntError),
    ParseFloatError(ParseFloatError),
    ParseIPError(AddrParseError),
    InvalidValue(String),
}

impl From<ureq::Error> for Error {
    fn from(e: ureq::Error) -> Self {
        Self::HTTPError(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Self::IOError(e)
    }
}

impl From<ParseFloatError> for Error {
    fn from(e: ParseFloatError) -> Self {
        Self::ParseFloatError(e)
    }
}

impl From<ParseIntError> for Error {
    fn from(e: ParseIntError) -> Self {
        Self::ParseIntError(e)
    }
}

impl From<AddrParseError> for Error {
    fn from(e: AddrParseError) -> Self {
        Self::ParseIPError(e)
    }
}
