pub fn parse_table(page: String) -> std::collections::HashMap<String, String> {
    let document = scraper::Html::parse_document(&page);
    let keys_selector = scraper::Selector::parse(".servizitable td:nth-child(1)").unwrap();
    let values_selector = scraper::Selector::parse(".servizitable td:nth-child(2)").unwrap();
    document
        .select(&keys_selector)
        .into_iter()
        .zip(document.select(&values_selector))
        .map(|x| {
            (
                (x.0).text().collect::<String>().trim().to_owned(),
                (x.1).text().collect::<String>().trim().to_owned(),
            )
        })
        .collect::<std::collections::HashMap<String, String>>()
}

pub fn parse_call_log(page: String) -> Vec<(String, String, String, String, String)> {
    let document = scraper::Html::parse_document(&page);
    let first_sel = scraper::Selector::parse(".edittable td:nth-child(1)").unwrap();
    let second_sel = scraper::Selector::parse(".edittable td:nth-child(2)").unwrap();
    let third_sel = scraper::Selector::parse(".edittable td:nth-child(3)").unwrap();
    let fourth_sel = scraper::Selector::parse(".edittable td:nth-child(4)").unwrap();
    let fifth_sel = scraper::Selector::parse(".edittable td:nth-child(5)").unwrap();
    document
        .select(&first_sel)
        .into_iter()
        .skip(2)
        .zip(document.select(&second_sel))
        .zip(document.select(&third_sel))
        .zip(document.select(&fourth_sel))
        .zip(document.select(&fifth_sel))
        .map(|x| {
            (
                ((((x.0).0).0).0)
                    .text()
                    .collect::<String>()
                    .trim()
                    .to_owned(),
                ((((x.0).0).0).1)
                    .text()
                    .collect::<String>()
                    .trim()
                    .to_owned(),
                (((x.0).0).1).text().collect::<String>().trim().to_owned(),
                ((x.0).1).text().collect::<String>().trim().to_owned(),
                (x.1).text().collect::<String>().trim().to_owned(),
            )
        })
        .collect()
}

#[test]
fn adsl_conn_info_parse() {
    let _ = r"";
}
